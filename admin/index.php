<?php

	require_once('../config.php');
	define('ADMIN', true);
	
	$Administration = new Administration();
	$Administration->handleRequests();
	
	if($Administration->isAdmin()){
		echo $Administration->showAdmin();
	}
	else {
		echo $Administration->showLogin();
	}

?>