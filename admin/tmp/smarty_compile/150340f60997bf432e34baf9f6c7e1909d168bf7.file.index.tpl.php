<?php /* Smarty version Smarty-3.1.19, created on 2015-05-17 17:01:40
         compiled from "../templates/admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:139952271955589f44ea9e81-34675641%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '150340f60997bf432e34baf9f6c7e1909d168bf7' => 
    array (
      0 => '../templates/admin/index.tpl',
      1 => 1409321724,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '139952271955589f44ea9e81-34675641',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bodyclass' => 0,
    'errors' => 0,
    'error' => 0,
    'content' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_55589f44ed34a1_17900150',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55589f44ed34a1_17900150')) {function content_55589f44ed34a1_17900150($_smarty_tpl) {?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>CMS Administration</title>
		<base href="http://localhost/cms/" >
		
		<!-- Bootstrap -->

		<script type="text/javascript" src="templates/admin/js/jquery-1.11.0.js"></script>
		<script type="text/javascript" src="templates/admin/js/bootstrap.min.js"></script>
		<link href="templates/admin/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="templates/admin/css/styles.css" rel="stylesheet" media="screen">
		<link href="templates/admin/css/sb-admin.css" rel="stylesheet" media="screen">
		<link href="templates/admin/fonts/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	</head>
	<body class="<?php echo $_smarty_tpl->tpl_vars['bodyclass']->value;?>
">
	
		<div class="container error_holder">
		<?php if (isset($_smarty_tpl->tpl_vars['errors']->value)&&count($_smarty_tpl->tpl_vars['errors']->value)) {?>
			<ul class="errors">
			<?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->_loop = true;
?>
				<li><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</li>
			<?php } ?>
			</ul>
		<?php }?>
		</div>
		
		<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

	</body>
</html><?php }} ?>
