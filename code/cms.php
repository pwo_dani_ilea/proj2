<?php

class CMS {

	protected static $errors = array();
	
	static function get($key) {
		if(isset($_REQUEST[$key])) {
			return $_REQUEST[$key];
		}
		return false;
	}
	
	static function getSession($key) {
		if(isset($_SESSION[$key])) {
			return $_SESSION[$key];
		}
		return false;
	}
	
	static function setSession($key, $value) {
		$_SESSION[$key] = $value;
	}
	
	static function addError($error) {
		self::$errors []= $error;
	}
	
	static function hasError() {
		return count(self::$errors) > 0;
	}
	
	static function renderSnippet($snippet, $assigns = array()) {
		$template = new Smarty();
				
		$template->assign('config', self::getSmartyConfig());
		
		if(defined('ADMIN')) {
			$template->setTemplateDir(ROOT.'code/views/admin/snippets/');	
		}
		else {
			$template->setTemplateDir(ROOT.'code/views/snippets/');
		}
		
		$template->setCompileDir(ROOT.'tmp/smarty_compile/');
		$template->setConfigDir(ROOT.'config/');
		$template->setCacheDir(ROOT.'tmp/cache/');
		
		$template->assign($assigns);
		
		$template->display($snippet.'.tpl');
	}
	
	public static function getSmartyConfig() {
	
		return array(
				"date" => DATE_FORMAT,
				"time" => TIME_FORMAT,
				"datetime" => DATE_TIME_FORMAT
		);
		
	}
	
	static function showPage($page, $assigns = array()) {
		$template = new Smarty();
				
		$template->assign('config', self::getSmartyConfig());
		
		if(defined('ADMIN')) {
			$template->assign('baseHref', 'http://'.$_SERVER['HTTP_HOST'].'/admin/');
			$template->setTemplateDir(ROOT.'code/views/admin/');	
			$template->assign('adminViewsPath', 'http://'.$_SERVER['HTTP_HOST'].'/code/views/admin');
		}
		else {
			$template->assign('baseHref', 'http://'.$_SERVER['HTTP_HOST']);
			$template->setTemplateDir(ROOT.'code/views/frontend/');
			$template->assign('frontViewsPath', 'http://'.$_SERVER['HTTP_HOST'].'/code/views/frontend');
		}
		
		$template->assign('site_address', 'http://'.$_SERVER['HTTP_HOST'].'/');
		
		$template->setCompileDir(ROOT.'tmp/smarty_compile/');
		$template->setConfigDir(ROOT.'config/');
		$template->setCacheDir(ROOT.'tmp/cache/');
//dump($assigns);
		$template->assign('errors', self::$errors);
		$template->assign($assigns);
		
		$template->assign('bodyclass', str_replace('/', ' ' , $page));
		
		$template->display($page.'.tpl');
	}
	
	static function getRenderedContent($content_type, $params = array()) {
		$template = new Smarty();
				
		$template->assign('config', self::getSmartyConfig());
		
		$template->setTemplateDir(ROOT.'code/views/frontend/content/');
		
		$template->setCompileDir(ROOT.'tmp/smarty_compile/');
		$template->setConfigDir(ROOT.'config/');
		$template->setCacheDir(ROOT.'tmp/cache/');
		
		$template->assign($params);
		
		ob_start();
		
		$template->display($content_type.'.tpl');
		
		$content = ob_get_clean();
		return $content;
	}
	
	static function redirect_admin($action, $params = array()) {
		if(count($params)) {
			foreach($params as $param_key => $param_val) {
				$action .= '&'.$param_key.'='.$param_val;
			}
		}
		header('Location: '.$_SERVER['HTTP_ORIGIN'].'/admin/index.php?action='.$action);
	}
	
	static function redirect($page) {
		if(is_int($page)) {
			$page = new Page($page);
		}
		/* 
		if(!$page->getActive()) {
			
		} */
		
		header('Location: '.$_SERVER['HTTP_ORIGIN'].'/'.$page->getUrl());
	}
	
	static function getSettings() {
		$settings = $GLOBALS['DB']->selectQuery('*', 'settings');
		return end($settings);
	}
	
	static function getSetting($setting) {
		$settings = $GLOBALS['DB']->selectQuery($setting, 'settings');
		if ($end = end($settings)) {
			return end($end);
		}
		return null;
	}
}

?>