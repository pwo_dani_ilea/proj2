<?php

class Administration {

	protected $content_types = array('text' => 'Text',
									 'text_image' => 'Text & Image',
									 'slider' => 'Image Slider',
									 'home_box' => 'Home Box',
									 'header1' => 'Header 1',
									 'hr_separator' => 'Separator',
									 );

	protected $column_sizes = array('1' => '1/12',
							   '2' => '2/12',
							   '3' => '3/12 (25%)',
							   '4' => '4/12',
							   '5' => '5/12',
							   '6' => '6/12 (50%)',
							   '7' => '7/12',
							   '8' => '8/12',
							   '9' => '9/12 (75%)',
							   '10' => '10/12',
							   '11' => '11/12',
							   '12' => '12/12 (100%)');
	
	public function isAdmin() {
		
		if($admin = $this->getAdmin()) {
	
			$result = $GLOBALS['DB']->selectQuery('*', 'admin', ' id = '.$admin->getId().' AND active = 1');
			if(count($result)) {
				return true;
			}
		}
	
		return false;
		
	}
	
	public function doAjaxToggleactive() {
		if(CMS::get('table') && CMS::get('id')) {
			$className = ucwords(CMS::get('table'));
			$obj = new $className(CMS::get('id'));
			if($obj && !is_null($obj->getId())) {
				$tmpRepo = new AbstractRepository($obj->getTable());
				
				if($obj->getActive()) {
					$obj->setActive(0);
				}
				else{
					$obj->setActive(1);
				}
				
				
				$obj = $tmpRepo->save($obj);
				CMS::renderSnippet('toggle_active', array('object' => $obj));
			}			
		}
	}
	
	public function doAjaxGetContentText() {
		if(CMS::get('id')) {
			$content = new Content(CMS::get('id'));
			
			if($content && !is_null($content->getId())) {
				echo $content->getText();
			}			
		}
		die();
	}
	
	public function doAjaxSaveContentText() {
		if(CMS::get('id')) {
			$content = new Content(CMS::get('id'));
			$content->setText(CMS::get('text'));
			$contentRepository = new ContentRepository();
			$contentRepository->save($content);
		}
		die();
	}
	
	public function doAjaxOrderPages() {
		if(CMS::get('order') && CMS::get('order')) {
			$pageRepository = new PageRepository();
			foreach(CMS::get('order') as $page_id => $page_position) {
				$tmpPage = new Page($page_id);
				if($tmpPage->getSorting() != $page_position) {
					$tmpPage->setSorting($page_position);
					$pageRepository->save($tmpPage, false);
				}
			}
			die(json_encode(true));	
		}
		die(json_encode(false));
	}
	
	public function doAjaxOrderContent() {
		if(CMS::get('order') && CMS::get('order')) {
			$contentRepository = new ContentRepository();
			foreach(CMS::get('order') as $content_id => $content_position) {
				$tmpContent = new Content($content_id);
				if($tmpContent->getSorting() != $content_position) {
					$tmpContent->setSorting($content_position);
					$contentRepository->save($tmpContent, false);
				}
			}
			die(json_encode(true));	
		}
		die(json_encode(false));
	}
	
	public function doAjaxRegenerateUrls() {
		$pageRepository = new PageRepository();
		$pages = $pageRepository->findAllHierarhical();
		$pageController = new PageController();
		
		foreach($pages as $page) {
			$page->setUrl($pageController->generateUrl($page));
			$pageRepository->save($page, false);
			
			if($page->getSubpages()) {
				foreach($page->getSubpages() as $subpage) {
					$subpage->setUrl($pageController->generateUrl($subpage));
					$pageRepository->save($subpage, false);
				}
			}
		}
		die(json_encode(true));	
		
	}
	
	public function handleRequests() {
		if(isset($_REQUEST['do'])) {
			switch($_REQUEST['do']) {
				
				case 'ajax':
					if($this->isAdmin() && isset($_REQUEST['action'])) {
						$actionName = 'doAjax'.ucwords(CMS::get('action'));
						if(method_exists('Administration', $actionName)) {
							$this->$actionName();
						}
					}
					die();
				break;
				
				case 'login':
					$this->login();
				break;
				
				case 'logout':
					$this->logout();
				break;
				
				case 'add_page':
					
					if($this->isAdmin()) {
						$pageController = new PageController();

						if($pageController->addPage()) {
							CMS::redirect_admin('pages');
						}
					}

				break;
				
				case 'edit_page':
					
					if($this->isAdmin()) {
						$pageController = new PageController();

						if($pageController->updatePage()) {
							CMS::redirect_admin('pages');
						}
					}

				break;
				
				case 'delete_page':
					
					if($this->isAdmin()) {
						$pageController = new PageController();

						if($pageController->delete()) {
							CMS::redirect_admin('pages');
						}
					}

				break;
				
				case 'restore_page':
					
					if($this->isAdmin()) {
						$historyController = new HistoryController();
						
						if($historyController->restore()) {
							CMS::redirect_admin('pages');
						}
					}

				break;
				
				case 'delete_history':
					
					if($this->isAdmin()) {
						$historyController = new HistoryController();

						if($historyController->delete()) {
							CMS::redirect_admin('history', array('table' => CMS::get('table'), 'objectid' => CMS::get('objectid')));
						}
					}

				break;
				
				case 'add_content':
					
					if($this->isAdmin()) {
						$contentController = new ContentController();
						
						$arrParams = array();
						if(CMS::get('page')) {
							$arrParams['page'] = CMS::get('page');
						}
						
						if($contentController->addContent()) {
							CMS::redirect_admin('content', $arrParams);
						}
					}

				break;
				
				case 'edit_content':
					
					if($this->isAdmin()) {
						$contentController = new ContentController();
						
						$arrParams = array();
						if(CMS::get('page')) {
							$arrParams['page'] = CMS::get('page');
						}
						
						if($contentController->updateContent()) {
							CMS::redirect_admin('content', $arrParams);
						}
					}

				break;
				
				case 'delete_content':
					
					if($this->isAdmin()) {
						$contentController = new ContentController();
						
						$arrParams = array();
						if(CMS::get('page')) {
							$arrParams['page'] = CMS::get('page');
						}

						if($contentController->delete()) {
							CMS::redirect_admin('content', $arrParams);
						}
					}

				break;
				
				case 'restore_content':
					
					if($this->isAdmin()) {
						$historyController = new HistoryController();
						
						$arrParams = array();
						if(CMS::get('page')) {
							$arrParams['page'] = CMS::get('page');
						}
						
						if($historyController->restore()) {
							CMS::redirect_admin('content', $arrParams);
						}
					}

				break;
				
				case 'copy_content':
					
					if($this->isAdmin() && CMS::get('id')) {
						$content = new Content(CMS::get('id'));
						if($content->getId()) {
							CMS::setSession('copy_content', $content->getId());
						}
					}

				break;
				
				case 'paste_content':
					
					if($this->isAdmin() && CMS::get('id')) {
						$content = new Content(CMS::get('id'));
						if($content->getId() && CMS::getSession('copy_content')) {
							$content = new Content(CMS::getSession('copy_content'));
							if($content && $content->getId()) {
								$contentController = new ContentController();
								$contentController->pasteContentAfter($content->getId(), CMS::get('id'));
								
								//CMS::setSession('copy_content', false);
							}
						}
					}

				break;
				
				case 'edit_settings':
					
					if($this->isAdmin()) {
						
						$this->updateSettings();
					
					}

				break;
				
			}
		}
	}
	
	public function updateSettings() {
										
		$db_settings = $GLOBALS['DB']->getFields('settings');

		$arrUpdateSettings = array();
		
		foreach($db_settings as $db_setting) {
			if(CMS::get($db_setting['Field']) !== false) {
				$arrUpdateSettings []= $db_setting['Field']." = \"".CMS::get($db_setting['Field'])."\""; 
			}
		}
		
		$GLOBALS['DB']->query('UPDATE settings SET '.implode(',',$arrUpdateSettings));
		
	}
	
	public function showAdmin() {
		if(CMS::get('action')) {
			$actionName = 'show'.ucwords(CMS::get('action'));
			if(method_exists('Administration', $actionName)) {
				return $this->$actionName();
			}
		}
		$this->showDashboard();
	}
	
	protected function logout() {
		CMS::setSession('admin', null);
	}
	
	public function showLogin() {
		CMS::showPage('login');
	}
	
	public function showDashboard() { 
		$assigns = array('admin' => $this->getAdmin());
		CMS::showPage('dashboard', $assigns);
	}
		
	public function getAdmin() {
		return CMS::getSession('admin');
	}
	
	public function login() {
		
		if(CMS::get('username') && CMS::get('password')) {
			
			$result = $GLOBALS['DB']->selectQuery('*', 
												  'admin', 
												  'username LIKE "'.CMS::get('username').'" AND 
												  password LIKE "'.md5(CMS::get('password')).'" AND 
												  active = 1');
													  
			if(count($result)) {
				$adminObj = new Admin($result[0]['id']);
				CMS::setSession('admin', $adminObj);
				return true;
			}
		}
		
		CMS::addError('Please input a valid username and password');
		
	}
	
	public function showPages() {
		$pageRepository = new PageRepository();
		$assigns = array('pages' => $pageRepository->findAllHierarhical());
		CMS::showPage('pages', $assigns);
	}
	
	public function showHistory() {
		$table = CMS::get('table');
		$className = ucwords($table);
		$id = CMS::get('objectid');
		$object = new $className($id);
		$historyRepository = new HistoryRepository();
		$assigns = array('history' => $historyRepository->findObjectHistory($object), 'object' => $object);
		CMS::showPage('history', $assigns);
	}
	
	public function showAddPage() {
		$pageRepository = new PageRepository();
		$assigns = array('page' => false, 'parents' => $pageRepository->findAllHierarhical(), 'history' => false);
		CMS::showPage('edit_page', $assigns);
	}
	
	public function showEditPage($page = null, $history = false) {
		if(is_null($page) && CMS::get('id')) {
			$page = new Page(CMS::get('id'));
		}
		if(!is_null($page) && !is_null($page->getId())) {
			$pageRepository = new PageRepository();
			CMS::showPage('edit_page', array('page' => $page, 'parents' => $pageRepository->findAllHierarhical(), 'history' => $history));
			return true;
		}
		
		$this->showAddPage();
	}
	
	public function showEditHistory() {
		if(CMS::get('id')) {
			$history = new History(CMS::get('id'));
			$history_object = $history->getObject();
			
			$selfMethodName = 'showEdit'.ucwords($history_object->getTable());
			$this->$selfMethodName($history_object, $history->getId());
			
		}
		else {
			CMS::addError('Missing history id');
		}
	}
	
	public function showContent() {
		$contentRepository = new ContentRepository();
		$pageRepository = new PageRepository();
		$selectablePages = $pageRepository->findAllHierarhical();
		
		if(CMS::get('page')) {
			$page = new Page(CMS::get('page'));
		}
		else {
			$page = isset($selectablePages[0]) ? $selectablePages[0] : null;
		}
		
		$copy_content = false;
		
		if(CMS::getSession('copy_content')) {
			$tmp_content = new Content(CMS::getSession('copy_content'));
			if($tmp_content->getId()) {
				$copy_content = $tmp_content->getId();
			}
		}
		
		$assigns = array('contents' => $contentRepository->findPageContent($page), 
						 'pages' => $selectablePages, 
						 'selected' => $page,  
						 'content_types' => $this->content_types, 
						 'column_sizes' => $this->column_sizes,
						 'copy_content' => $copy_content);
		CMS::showPage('content', $assigns);
	}
	
	public function showAddContent() {
		
		$pageRepository = new PageRepository();
		$selectablePages = $pageRepository->findAllHierarhical();
		
		if(CMS::get('page')) {
			$page = new Page(CMS::get('page'));
		}
		else {
			$page = isset($selectablePages[0]) ? $selectablePages[0] : null;
		}
		
		$assigns = array('content' => false, 
						 'pages' => $selectablePages, 
						 'selected' => $page,
						 'content_types' => $this->content_types, 
						 'column_sizes' => $this->column_sizes, 
						 'history' => false);
		
		CMS::showPage('edit_content', $assigns);
	}
	
	public function showEditContent($content = null, $history = false) {
		if(is_null($content) && CMS::get('id')) {
			$content = new Content(CMS::get('id'));
		}
		if(!is_null($content) && !is_null($content->getId())) {
			$pageRepository = new PageRepository();
			$assigns = array('content' => $content, 
							 'pages' => $pageRepository->findAllHierarhical(), 
							 'content_types' => $this->content_types, 
							 'column_sizes' => $this->column_sizes, 
							 'history' => $history);
			CMS::showPage('edit_content', $assigns);
			return true;
		}
		
		$this->showAddContent();
	}
	
	public function showSettings() {
		$settings = CMS::getSettings();
		$pageRepo = new PageRepository();
		CMS::showPage('settings', array('settings' => $settings, 'pages' => $pageRepo->findAllHierarhical()));
	}
}

?>