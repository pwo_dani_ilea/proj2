<?php 

class ContentController {
	
	public function addContent() {
	
		$this->validateFromPost();	
		if(!CMS::hasError()) {
			$content = new Content();
			$content->setActive(CMS::get('active') ? 1 : 0);
			$content->setPage(CMS::get('page'));
			$content->setName(CMS::get('name'));
			$content->setShowname(CMS::get('showname'));
			$content->setType(CMS::get('type'));
			$content->setColumns(CMS::get('columns'));
			$content->setText(CMS::get('text'));
						
			$contentRepository = new ContentRepository();
			$contentRepository->save($content);
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public function delete($content = null) {
		if(!is_null($text)) {
			$contentRepository = new ContentRepository();
			$contentRepository->delete($content);
		}
		elseif(CMS::get('id')) {
			$content = new Content(CMS::get('id'));
			$contentRepository = new ContentRepository();
			$contentRepository->delete($content);
			return true;
		}
		else {
			CMS::addError('Content id is mandatory.');
			return false;
		}
		
	}
	
	public function validateFromPost() {
		if(!CMS::get('page')) {
			CMS::addError('Page is mandatory.');
		}
			
		if(!CMS::get('name')) {
			CMS::addError('Title is mandatory.');
		}
			
		if(!CMS::get('type')) {
			CMS::addError('Type is mandatory.');
		}
	}
	
	public function updateContent() {
		$this->validateFromPost();		
		if(!CMS::hasError()) {
			$content = new Content(CMS::get('id'));
			if(!is_null($content->getId())) {
				$content->setActive(CMS::get('active') ? 1 : 0);
				$content->setPage(CMS::get('page'));
				$content->setName(CMS::get('name'));
				$content->setShowname(CMS::get('showname'));
				$content->setType(CMS::get('type'));
				$content->setColumns(CMS::get('columns'));
				$content->setText(CMS::get('text'));
							
				$contentRepository = new ContentRepository();
				$contentRepository->save($content);
				return true;
			}
			else {
				CMS::addError('Invalid Content Id');
			}
		}
		return false;
	}
	
	public function renderContent($content) {
		if($content && !is_null($content->getId())) {
			$administration = new Administration();
			if($administration->isAdmin()) {
				return CMS::getRenderedContent($content->getType(), array('content' => $content, 'edit' => true));
			}
			else {
				return CMS::getRenderedContent($content->getType(), array('content' => $content, 'edit' => false));
			}
		}
	}
	
	public function pasteContentAfter($pasted_id, $after_id) {
	
		$pasted = new Content($pasted_id);
		$after = new Content($after_id);
		
		$contentRepository = new ContentRepository();
		
		$pasted->setId(null);
		$pasted->setPage($after->getPage());
		$pasted->setSorting($after->getSorting());
		
		$exists = true;
		$counter = 1;
		while($exists != false) {
			
			$copy_name = $pasted->getName(). ' (copy '.$counter.')';
						
			$row = $GLOBALS['DB']->selectQuery('count(*) as c', 'content', 'name LIKE "'.$copy_name.'" AND page = '.$pasted->getPage());
			
			if($row[0]['c'] == 0 || $counter == 5) {
				$pasted->setName($copy_name);
				$exists = false;
				break;
			}
			$counter++;	
		}
	
		$contentRepository->save($pasted, true, true);
	}
}

?>