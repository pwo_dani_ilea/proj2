<?php

class Frontend {
	
	public function render() {
	
		$pageController = new PageController();
		
		$page = $this->getPage();
		if(!is_null($page)) {
			$contentRepo = new ContentRepository();
			$assigns = array();
			$assigns['page'] = $page;
			$contentController = new ContentController();
			$contents = $contentRepo->getActivePageContent($page);
			$assigns['content'] = '';
			foreach($contents as $tmpContent) {
				$assigns['content'] .= $contentController->renderContent($tmpContent);
			}
			$assigns['settings'] = CMS::getSettings();
			$assigns['homepage'] = $pageController->getHomePage();
			$assigns['site_address'] = $pageController->getHomePage();
			$assigns['menu_pages'] = $pageController->getMenuPages();
			
			$administration = new Administration();
			if($administration->isAdmin()) {
				$assigns['edit'] = true;
			}
			else {
				$assigns['edit'] = false;
			}
			
			CMS::showPage('index', $assigns);
		}
		else {
			die('No page exists yet.');
		}
		
	}
	
	public function getPage() {
		$page = null;
		
		$url = trim($_SERVER['REQUEST_URI']);
		$url = substr($url, 1, strlen($url));
		if(substr($_SERVER['REQUEST_URI'], -1, 1) == "/") {
			$url = substr($url, 0, strlen($url) - 1);
		}
		
		$pageController = new PageController();
			
		if(strlen($url) > 0) {
			$page = $pageController->findByUrl($url);
			if(is_null($page)) {
				$page = $pageController->getMissingPage();
			}
		}
		else {
			$page = $pageController->getHomePage();
		}
		
		return $page;
	}
	
	
}