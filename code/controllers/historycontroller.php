<?php

class HistoryController {

	public function delete($history = null) {
		if(!is_null($history)) {
			$historyRepository = new HistoryRepository();
			$historyRepository->delete($history);
			return true;
		}
		elseif(CMS::get('id')) {
			$history = new History(CMS::get('id'));
			$historyRepository = new HistoryRepository();
			$historyRepository->delete($history);
			return true;
		}
		else {
			CMS::addError('History id is mandatory.');
			return false;
		}
		
	}

	public function restore($history = null) {
		if(!is_null($history)) {
			$old_object = $history->getObject();
			
			$repo = new AbstractRepository();
			$repo->save($old_object);
			
			return true;
		}
		elseif(CMS::get('id')) {
			$history = new History(CMS::get('id'));
			$old_object = $history->getObject();
			
			$repo = new AbstractRepository();
			$repo->save($old_object);
			
			return true;
		}
		else {
			CMS::addError('History id is mandatory.');
			return false;
		}
		
	}

}
?>