<?php 

class PageController {
	
	public function addPage() {
				
		if(CMS::get('name') && strlen(trim(CMS::get('name')))) {
			$pageName = $this->normalizePageName(CMS::get('name'));
			if(strlen($pageName) == 0) {
				CMS::addError('Page name is not correct.');
				return false;
			}
			
			$page = new Page();
			$page->setActive(CMS::get('active') ? 1 : 0);
			$page->setName($pageName);
			$page->setParent(CMS::get('parent'));
			
			$url = $this->generateUrl($page);
			$page->setUrl($url);
			
			$pageRepository = new PageRepository();
			$pageRepository->save($page);
			return true;
		}
		else {
			CMS::addError('Page name is mandatory.');
			return false;
		}
		
	}
	
	public function normalizePageName($string) {
		return preg_replace('/[^\p{L}\p{N}\s]/u', '', trim($string));
	}
	
	public function generateUrl($page) {
		$page_url = str_replace(' ', '_',strtolower(trim(preg_replace('/[^a-zA-Z0-9 ]/u', '', trim(preg_replace ("/ +/", " ",$page->getName()))))));
		
		if($page->getParent()) {
			$parentPage = new Page($page->getParent());
			$page_url = $parentPage->getUrl().'/'.$page_url;
		}
		
		$exists = true;
		$counter = 0;
		while($exists != false) {
			if($counter > 0) {
				$url_search = $page_url.$counter;
			}
			else {
				$url_search = $page_url;
			}
			if(!is_null($page->getId())) {
				$row = $GLOBALS['DB']->selectQuery('count(*) as c', 'page', 'url LIKE "'.$url_search.'" AND id != "'.$page->getId().'"');
			}
			else {
				$row = $GLOBALS['DB']->selectQuery('count(*) as c', 'page', 'url LIKE "'.$url_search.'"');
			}
			if($row[0]['c'] == 0) {
				$page_url = $url_search;
				break;
			}
			$counter++;	
		}
		
		return $page_url;
	}
	
	public function delete($page = null) {
		if(!is_null($page)) {
			$pageRepository = new PageRepository();
			$pageRepository->delete($page);
		}
		elseif(CMS::get('id')) {
			$page = new Page(CMS::get('id'));
			$pageRepository = new PageRepository();
			$pageRepository->delete($page);
			return true;
		}
		else {
			CMS::addError('Page id is mandatory.');
			return false;
		}
		
	}
	
	public function updatePage() {
		
		if(!CMS::get('id')) {
			CMS::addError('Missing Page Id');
			return false;
		}
				
		if(CMS::get('name') && strlen(trim(CMS::get('name')))) {
			$page = new Page(CMS::get('id'));
			if(!is_null($page->getId())) {
			
				$pageName = $this->normalizePageName(CMS::get('name'));
				
				if(strlen($pageName) == 0) {
					CMS::addError('Page name is not correct.');
					return false;
				}
				
				$page->setActive(CMS::get('active') ? 1 : 0);
				$page->setName($pageName);
				$page->setParent(CMS::get('parent'));
				
				$url = $this->generateUrl($page);
				$page->setUrl($url);
				
				$pageRepository = new PageRepository();
				$pageRepository->save($page);
				
				if($pageRepository->findAllHierarhical($page->getId())) {
					foreach($pageRepository->findAllHierarhical($page->getId()) as $subpage) {
						$subpage->setUrl($this->generateUrl($subpage));
						$pageRepository->save($subpage);
					}
				}
				
				return true;
			}
			else {
				CMS::addError('Invalid Page Id');
				return false;
			}
		}
		else {
			CMS::addError('Page name is mandatory.');
		}
	}
	
	public function getHomePage() {
		$pageRepo = new PageRepository();
		$homePage = null;
		if(CMS::getSetting('home_page')) {
			$homePage = new Page(CMS::getSetting('home_page'));
			if(!is_null($homePage->getId()) && $homePage->getActive()) {
				return $homePage;
			}
		}
			
		$firstPage = $pageRepo->findFirst('ASC', 'active = 1');
		if($firstPage && $firstPage->getActive()) {
			return $firstPage;
		}
			
		return null;
	}
	
	public function findByUrl($url) {
		$pageRepo = new PageRepository();
		$page = null;
		
		$row = $GLOBALS['DB']->selectQuery('*', 'page', 'url LIKE "'.$url.'" AND active = 1');
		
		if(count($row)) {
			$page = new Page($row[0]['id']);
		}
		return $page;
	}
	
	public function getMissingPage() {
		$pageRepo = new PageRepository();
		$missingPage = null;
		if(CMS::getSetting('missing_page')) {
			$missingPage = new Page(CMS::getSetting('missing_page'));
			if(!is_null($missingPage->getId()) && $missingPage->getActive()) {
				return $missingPage;
			}
		}
			
		return $missingPage;
	}
	
	public function getMenuPages() {
		$arrPages = array();
		$pageRepo = new PageRepository();
		$missingPage = CMS::getSetting('missing_page');
		
		$pages = $pageRepo->findAllHierarhical(0, 'ASC');
		
		foreach($pages as $key => $tmpPage) {
			if(!$tmpPage->getActive() || ($missingPage && $tmpPage->getId() == $missingPage)) {
				continue;
			}
			else{
				$arrPages[$key] = $tmpPage;
				if($tmpPage->getSubpages()) {
					foreach($tmpPage->getSubpages() as $subkey => $tmpSubpage) {
						if(!$tmpSubpage->getActive() || ($missingPage && $tmpSubpage->getId() == $missingPage)) { 
							continue;
						}
						else {
							$arrPages[$key]->sub_pages[$subkey] = $tmpSubpage;
						}
					}
				}
			}
		}
			
		return $arrPages;
	}
	
}

?>