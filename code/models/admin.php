<?php

class Admin extends Model {
	
	protected $name;
	protected $username;
	protected $password;
	protected $last_logged_in;
	
	public function __construct($id) {
		parent::__construct('admin', $id);
	}

	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
	}

	public function getUsername() {
		return $this->username;
	}
	
	public function setUsername($username) {
		$this->username = $username;
	}

	public function getPassword() {
		return $this->password;
	}
	
	public function setPassword($password) {
		$this->password = $password;
	}
	
	public function getLastLoggedIn(){ 
		return $this->last_logged_in;
	}
	
	public function setLastLoggedIn($last_logged_in){ 
		$this->last_logged_in = $last_logged_in;
	}
	
}

?>