<?php

class Content extends Model {
	
	protected $name;
	protected $showname;
	protected $page;
	protected $text;
	protected $type;
	protected $sorting;
	protected $columns;
	
	public function __construct($id = null) {
		parent::__construct('content', $id);		
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getShowname() {
		return $this->showname;
	}
	
	public function setShowname($showname) {
		$this->showname = $showname;
	}
	
	public function getPage() {
		return $this->page;
	}
	
	public function setPage($page) {
		$this->page = $page;
	}
	
	public function getText() {
		return $this->text;
	}
	
	public function setText($text) {
		$this->text = $text;
	}	
	
	public function getType() {
		return $this->type;
	}
	
	public function setType($type) {
		$this->type = $type;
	}	
	
	public function getSorting() {
		return $this->sorting;
	}
	
	public function setSorting($sorting) {
		$this->sorting = $sorting;
	}
	
	public function getColumns() {
		return $this->columns;
	}
	
	public function setColumns($columns) {
		$this->columns = $columns;
	}
	
}

?>