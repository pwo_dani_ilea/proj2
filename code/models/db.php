<?php

class DB extends mysqli {
	
    private static $instance = null;
	public $debugQuery = false;
	
	public static function getInstance() {
		if (!self::$instance instanceof self) {
			self::$instance = new self;
		}
		return self::$instance;
	}
	
	public function escape($string) {
		return $this->real_escape_string($string);
	}
	
	/*
	Establish the database connection 
	*/
	
	private function __construct() {
		parent::__construct(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if (mysqli_connect_error()) {
			exit('Connect Error (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		parent::set_charset('utf-8');
	}
	
	/*
	Creates a SELECT database query and returns the resulted rows
	*/
	public function selectQuery($fields, $from_table, $where = '', $group_by = '', $order_by = '', $limit = '') {
		
		$arrResults = array();
		
		if($where) {
			//$where = ' AND '.$this->real_escape_string($where);
			$where = ' AND '.$where;
		}
				
		$queryString = 'SELECT '.$fields.' FROM '.$from_table.' WHERE 1 = 1 '.$where.' ';
		
		if($group_by) {
			$queryString .= ' GROUP BY '.$group_by.' ';
		}
		
		if($order_by) {
			$queryString .= ' ORDER BY '.$order_by.' ';
		}
		
		if($limit) {
			$queryString .= ' LIMIT '.$limit.' ';
		}
		
		if($this->debugQuery) {
			dump($queryString);
		}
		
		$query = $this->query($queryString);
		
		if($query) {
			
			while($row = $query->fetch_assoc()) {
				$arrResults []= $row;
			}
		}
		elseif(DEV_MODE) {
			var_dump('Error: '.$this->error);
			var_dump('Query: '.$queryString);
			return false;
		}
		
		
		return $arrResults;
	}
	
	public function update($table, $id, $fields) {
		
		$fieldsString = "";
		$tmpFieldsArray = array();
		
		foreach($fields as $field => $value) {
			$tmpFieldsArray []= $field . ' = "'.$this->escape($value).'"'; 
		}
		
		$fieldsString = implode(",", $tmpFieldsArray);
	
		$queryString = 'UPDATE '.$table.' SET '.$fieldsString.' WHERE id = '.$id;
		
		if($this->debugQuery) {
			dump($queryString);
		}
		
		$query = $this->query($queryString);
		
		if($query) {
			
			return true;
		}
		elseif(DEV_MODE) {
			var_dump('Error: '.$this->error);
			var_dump('Query: '.$queryString);
		}
		
		return false;
	}
	
	public function insert($table, $fields) {
		
		$fieldNamesArray = array();
		$fieldValuesArray = array();
		
		
		foreach($fields as $field => $value) {
			$fieldNamesArray []= $this->escape($field); 
			$fieldValuesArray []= "'".$this->escape($value)."'"; 
		}
		
		$fieldNamesString = implode(",", $fieldNamesArray);
		$fieldValuesString = implode(",", $fieldValuesArray);
	
		$queryString = 'INSERT INTO '.$table.' ('.$fieldNamesString.') VALUES ('.$fieldValuesString.')';
		//dump($queryString);
		//die();
		
		if($this->debugQuery) {
			dump($queryString);
		}
		
		$query = $this->query($queryString);
		
		if($query) {
			return $this->insert_id;
		}
		elseif(DEV_MODE) {
			var_dump('Error: '.$this->error);
			var_dump('Query: '.$queryString);
		}
		
		return false;
	}
	
	public function delete($table, $id) {
		
		$queryString = "DELETE FROM ".$table." WHERE id = ".$id;
		
		if($this->debugQuery) {
			dump($queryString);
		}
		
		$query = $this->query($queryString);
		
		if($query) {
			return true;
		}
		
		return false;
	}
	
	public function deleteWhere($table, $where) {
	
		$queryString = "DELETE FROM ".$table." WHERE ".$where;
		
		if($this->debugQuery) {
			dump($queryString);
		}
		
		$query = $this->query($queryString);
		
		if($query) {
			return true;
		}
		
		return false;
	}
	
	public function getFields($table) {
		
		$arrResults = array();
		
		$queryString = "SHOW COLUMNS FROM ".$table;
		
		if($this->debugQuery) {
			dump($queryString);
		}
		
		$query = $this->query($queryString);
		
		if($query) {
			while($row = $query->fetch_assoc()) {
				$arrResults []= $row;
			}
			return $arrResults;
		}
		elseif(DEV_MODE) {
			var_dump('Error: '.$this->error);
			var_dump('Query: '.$queryString);
		}
		
		return false;
	}
		
	public function find($id, $class_name) {
		return new $class_name($id);
	}
}

?>