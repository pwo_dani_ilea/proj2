<?php

class History extends Model {
	
	protected $tablename;
	protected $objectid;
	protected $data;
	protected $fields;
	
	public function __construct($id = null) {
		parent::__construct('history', $id);		
	}
	
	public function getObject() {
		$object = null;
		if($this->data) {
			$object = unserialize($this->data);
		}
		return $object;
	}
	
	public function setObject($object, $oldObject = null) {	
		$changed_fields = array();
		$methods = get_class_methods($object);
		foreach($methods as $method) {
			if(substr($method, 0, 3) == 'get') {
				$field = strtolower(substr($method, 3, strlen($method)));
				if(!in_array($field, array('id', 'table', 'created', 'modified', 'sorting'))) {
					if($object->$method() != $oldObject->$method()) {
						$changed_fields []= $field;
					}
				}
			}
		}
		if(count($changed_fields)) {
			$this->fields = implode(',', $changed_fields);
		}
		$this->tablename = $object->getTable();
		$this->objectid = $object->getId();
		$this->data = serialize($object);
	}
	
	public function getTablename() {
		return $this->tablename;
	}
	
	public function setTablename($tablename) {
		$this->tablename = $tablename;
	}
	
	public function getObjectid() {
		return $this->objectid;
	}
	
	public function setObjectid($objectid) {
		$this->objectid = $objectid;
	}
	
	public function getData() {
		return $this->data;
	}
	
	public function setData($data) {
		$this->data = $data;
	}
	
	public function getFields() {
		return $this->fields;
	}
	
	public function setFields($fields) {
		$this->fields = $fields;
	}
		
}

?>