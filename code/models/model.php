<?php

abstract class Model {
	protected $id;
	protected $table;
		
	protected $active;
	protected $created;	
	protected $modified;
	
	public function __construct($table, $id = null) {		
		$this->table = $table;
		if(!is_null($id)) {
			$tmpObject = $GLOBALS['DB']->selectQuery('*', $this->table, ' id = '.$id);
			if(count($tmpObject)){
				$objParameters = array_keys($tmpObject[0]);
				foreach($objParameters as $parameter) {
					$this->$parameter = $tmpObject[0][$parameter];
				}
			}			
		}
	}
	
	public function populateFromPost() {
		foreach($_POST as $key => $value) {
			if(property_exists($this, $key)) {
				$this->$key = $value;
			}
		}
	}
	
	public function getId() {		
		return $this->id;	
	}
	
	public function setId($id) {		
		$this->id = $id;	
	}
	
	public function getActive() {		
		return $this->active;	
	}
	
	public function setActive($active) {		
		$this->active = $active;	
	}
	
	public function getCreated() {		
		return $this->created;	
	}
	
	public function setCreated($created) {	
		$this->created = $created;	
	}
	
	public function getModified() {		
		return $this->modified;	
	}
	
	public function setModified($modified) {		
		$this->modified = $modified;	
	}
	
	public function getTable() {
		return $this->table;
	}
}

?>