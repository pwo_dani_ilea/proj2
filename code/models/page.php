<?php

class Page extends Tree {
	
	protected $name;
	protected $sorting;
	protected $subpages = array();
	protected $parent;
	protected $url;
	
	public function __construct($id = null) {
		parent::__construct('page', $id);
		
		if(!is_null($this->id) && !$this->parent) {
			$tmpsubpageIDs = $GLOBALS['DB']->selectQuery('id', 'page', 'parent = '.$this->id);
			foreach($tmpsubpageIDs as $subPageId) {
				$this->subpages []= new Page($subPageId['id']);
			}
		}
		
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getParent() {
		return $this->parent;
	}
	
	public function setParent($parent) {
		$this->parent = $parent;
	}
	
	public function getSorting() {
		return $this->sorting;
	}
	
	public function setSorting($sorting) {
		$this->sorting = $sorting;
	}
	
	public function getSubpages() {
		return $this->subpages;
	}
	
	public function setSubpages($subpages) {
		$this->subpages = $subpages;
	}
	
	public function getUrl() {
		return $this->url;
	}
	
	public function setUrl($url) {
		$this->url = $url;
	}
	
	
}

?>