<?php

class AbstractRepository {
	protected $table;
	
	public function __construct($table) {
		$this->table = $table;
	}
	
	public function findAll($order = 'ASC', $extraWhere = '', $limit = '') {
		$arrResults = array();
		$className = ucwords($this->table);
		$arrIds = $GLOBALS['DB']->selectQuery('id', $this->table, $extraWhere, '', 'sorting '.$order. ', id '.$order . ' ' . $limit);
		
		foreach($arrIds as $tmpId) {
			$arrResults []= new $className($tmpId['id']);
		}
		
		return $arrResults;
	}
	
	public function findFirst($order = 'ASC', $extraWhere) {
		$result = $this->findAll($order, $extraWhere, ' LIMIT 1 ');
		if(count($result)) {
			return $result[0];
		}
		return false;
	}
	
	public function delete($object) {
		$this->table = $object->getTable();
		if(!is_null($object->getId())) {
			$GLOBALS['DB']->delete($this->table, $object->getId());
			$GLOBALS['DB']->deleteWhere('history', 'tablename = "'.$this->table.'" AND objectid = "'.$object->getId().'"');
		}
	}
	
	public function save($object, $add_to_history = true) {
		
		$this->table = $object->getTable();
		$className = ucwords($this->table);
		$tableFields = $GLOBALS['DB']->getFields($this->table);
		
		$updateFields = array();
		$updateId = $object->getId();
		
		$current_time = time();
		
		$object->setModified($current_time);
		if(is_null($object->getId())) {
			$object->setCreated($current_time);
		}
			
		foreach($tableFields as $tmpField) {
			$fieldName = $tmpField['Field'];
			if(!in_array($fieldName, array('id'))) {
				$getterName = 'get'.ucwords($fieldName);
				if(property_exists($object, $fieldName) && method_exists($object, $getterName)) {
					if(in_array($fieldName, array('text', 'data'))) {
						$updateFields[$fieldName] = $object->$getterName();
					}
					else {
						$updateFields[$fieldName] = strip_tags($object->$getterName());
					}
				}
			}
		}
		
		if(count($updateFields)) {
			if($add_to_history) {
				$oldObject = new $className($object->getId());
			}
			if(is_null($object->getId())) {
				$object_id = $GLOBALS['DB']->insert($this->table, $updateFields);
				if(is_int($object_id)) {
					$object = new $className($object_id);
				}
			}
			else {
				$GLOBALS['DB']->update($this->table, $object->getId(), $updateFields);
			}
			
			if($add_to_history) {
				$historyObject = new History();
				$historyObject->setObject($object, $oldObject);
				$historyRepository = new HistoryRepository();
				$historyRepository->save($historyObject);
			}
			
		}
		
		return $object;
	}
}

?>