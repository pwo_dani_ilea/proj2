<?php

class ContentRepository extends AbstractRepository{
		
	public function __construct() {
		parent::__construct('content');
	}
	
	public function findPageContent($page = null, $extraWhere = '') {
		$arrResults = array();
		
		if(is_null($page) && CMS::get('page')) {
			$page = new Page(CMS::get('page'), true);
		}
		if(!is_null($page) && !is_null($page->getId())) {
			$arrResults = $this->findAll('ASC', ' page = '.$page->getId(). ' ' . $extraWhere);
		}
		
		return $arrResults;
	}
	
	public function getActivePageContent($page) {
		return $this->findPageContent($page, ' AND active = 1');
	}
	
	public function save($object, $strict = true, $ignore_position = false) {
		if(is_null($object->getId()) && !$ignore_position) {
			
			$maxpos = $GLOBALS['DB']->selectQuery('MAX(sorting) as maxpos','content','page = '.$object->getPage());
			
			if($maxpos && !is_null($maxpos[0]['maxpos'])) {
				$pos = $maxpos[0]['maxpos'] + 1;
			}
			else {
				$pos = 0;
			}
			
			$object->setSorting($pos);
			
		}
		parent::save($object, $strict);
	}
	
}
?>