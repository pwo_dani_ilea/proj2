<?php

class HistoryRepository extends AbstractRepository{
		
	public function __construct() {
		parent::__construct('history');
	}
	
	public function findObjectHistory($object) {
		$arrResults = array();
		$className = ucwords($this->table);
		$arrIds = $GLOBALS['DB']->selectQuery('id', $this->table, 'tablename = "'.$object->getTable().'" AND objectid = "'.$object->getId().'"', '', 'modified desc ');
		
		foreach($arrIds as $tmpId) {
			$arrResults []= new $className($tmpId['id']);
		}
		
		return $arrResults;
	}
	
	public function save($object, $strict_standards = true) {
		parent::save($object, false);
	}
}
?>