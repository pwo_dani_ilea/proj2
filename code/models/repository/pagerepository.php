<?php

class PageRepository extends AbstractRepository{
		
	public function __construct() {
		parent::__construct('page');
	}
	
	public function findAllHierarhical($parent = 0, $order = 'ASC') {
		$arrResults = $this->findAll($order, ' parent = '.$parent);
		foreach($arrResults as &$tmpResult) {
			$tmpChildren = $this->findAllHierarhical($tmpResult->getId(), $order);
			if(count($tmpChildren)) {
				$tmpResult->setSubpages($tmpChildren);
				
			}
		}
		return $arrResults;
	}
	
	public function save($object, $strict = true) {
		
		if(is_null($object->getId())) {
			
			$maxpos = $GLOBALS['DB']->selectQuery('MAX(sorting) as maxpos','page','parent = '.$object->getParent());
			
			if($maxpos && !is_null($maxpos[0]['maxpos'])) {
				$pos = $maxpos[0]['maxpos'] + 1;
			}
			else {
				$pos = 0;
			}
			
			$object->setSorting($pos);
			
		}
		parent::save($object, $strict);
	}
	
	public function delete($object) {
		if($object->getSubpages()) {
			foreach($object->getSubpages() as $tmp_page) {
				$this->delete($tmp_page);
			}
		}
		parent::delete($object);
	}
	
}
?>