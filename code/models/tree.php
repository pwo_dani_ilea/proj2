<?php

class Tree extends Model {
	protected $parent;
	
	public function __construct($table, $id = null) {
		parent::__construct($table, $id);
	}
	
	public function getParent() {
		return $this->parent;
	}
	
	public function setParent($parent) {
		$this->parent = $parent;
	}
	
	public function getParentObject() {
		$parent_id = $this->getParent();
		if(!is_null($parent_id)) {
			$className = ucwords($this->getTable());
			$tmpParent = new $className($parent_id);
			if(!is_null($tmpParent->getId())) {
				return $tmpParent;
			}
		}
	}
	
	public function hasChildren() {
		
	}
}

?>