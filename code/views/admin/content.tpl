{extends file='dashboard.tpl'}

{block name=inner_content}
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Content {if $selected} for page '{$selected->getName()}'{/if}
				<a class="add_new" href="index.php?action=addContent{if $selected}&page={$selected->getId()}{/if}">Add New <i class="fa fa-plus fa-fw"></i></a>
			</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<div class="row">
		<div class="col-lg-12">
			<form id="change_page" action="index.php">
				<input type="hidden" name="action" value="content" />
				{if $selected}
					{$selected_id = $selected->getId()}
				{else}
					{$selected_id = null}
				{/if}
				{include file="snippets/select_page.tpl" pages=$pages selected=$selected_id}
			</form>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables">
							<thead>
								<tr>
									<th>ID</th>
									<th>Active</th>
									<th>Title</th>
									<th>Type</th>
									<th>Size</th>
									<th>Created</th>
									<th>Modified</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>								
								{foreach from=$contents item=content name=list_content}
									<tr class="content page {if $smarty.foreach.list_content.index %2}my_odd{else}my_even{/if}" data-page="{$content->getId()}">
										<td>{$content->getId()}</td>
										<td>{include file="snippets/toggle_active.tpl" object=$content}</td>
										<td class="center">{$content->getName()}</td>
										<td class="center">{if $content->getType() && isset($content_types[$content->getType()])}{$content_types[$content->getType()]}{/if}</td>
										<td class="center">{if $content->getColumns() && isset($column_sizes[$content->getColumns()])}{$column_sizes[$content->getColumns()]}{/if}</td>
										<td>{$content->getCreated()|date_format:$config.datetime}</td>
										<td>{$content->getModified()|date_format:$config.datetime}</td>
										<td class="center actions">
											<a href="index.php?action=editContent&id={$content->getId()}" class="edit_entry" title="Edit Content"> <i class="fa fa-pencil-square-o"></i></a> 
											<form class="delete_object" method="post">
												<input type="hidden" name="do" value="delete_content" />
												<input type="hidden" name="id" value="{$content->getId()}" />
												<a class="delete_entry" title="Delete Content"> <i class="fa fa-times"></i></a>
											</form>
											<a href="index.php?action=history&table=content&objectid={$content->getId()}" class="history_entry" title="See Content History"><i class="fa fa-history"></i></a> 
											<form class="copy_object" method="post">
												<input type="hidden" name="do" value="copy_content" />
												<input type="hidden" name="id" value="{$content->getId()}" />
												<a class="copy_entry" title="Copy Content"> <i class="fa fa-files-o"></i></a>
											</form>
											
											{if $copy_content}
											<form class="paste_object" method="post">
												<input type="hidden" name="do" value="paste_content" />
												<input type="hidden" name="id" value="{$content->getId()}" />
												<a class="paste_entry" title="Paste Content After"> <i class="fa fa-clipboard"></i></a>
											</form>
											{/if}
											
											{if count($contents) > 1}
												<span class="reorder_holder">
												{if !$smarty.foreach.list_content.last}
													<a class="move_down" data-id="{$content->getId()}" title="Move Down"><i class="fa fa-arrow-down"></i></a>
												{/if}
												{if !$smarty.foreach.list_content.first}
													<a class="move_up" data-id="{$content->getId()}" title="Move Up"><i class="fa fa-arrow-up"></i></a>
												{/if}
												</span>
											{/if}
										</td>
									</tr>
								{/foreach}
							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
					
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
{/block}