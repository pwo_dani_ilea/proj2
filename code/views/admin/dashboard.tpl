{extends file='index.tpl'}

{block name=dashboard}
<div id="wrapper">

	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="">Content Management System</a>
		</div>
		<!-- /.navbar-header -->

		<ul class="nav navbar-top-links navbar-right">
			<!-- /.dropdown -->
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-user">
					{*}
					<li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
					</li>
					{*}
					<li><a href="index.php?action=settings"><i class="fa fa-gear fa-fw"></i> Settings</a>
					</li>
					<li class="divider"></li>
					<li>
						<form method="post" id="logout_form">
							<a id="admin_logout" type="submit" href="#">
								<i class="fa fa-sign-out fa-fw"></i> Logout
							</a>
							<input type="hidden" name="do" value="logout" />
						</form>
					</li>
				</ul>
				<!-- /.dropdown-user -->
			</li>
			<!-- /.dropdown -->
		</ul>
		<!-- /.navbar-top-links -->

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
				<ul class="nav" id="side-menu">
					<li class="sidebar-search">
						<div class="input-group custom-search-form">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<button class="btn btn-default" type="button">
								<i class="fa fa-search"></i>
							</button>
						</span>
						</div>
						<!-- /input-group -->
					</li>
					<li class="dashboard">
						<a href="index.php"><i class="fa fa-tachometer"></i> Dashboard</a>
					</li>
					<li class="pages">
						{$active_classes = ['pages', 'edit_page']}
						<a {if in_array($bodyclass, $active_classes)}class="active"{/if} href="index.php?action=pages"><i class="fa fa-files-o"></i> Pages</a>
					</li>
					<li class="content">
						{$active_classes = ['content', 'edit_content']}
						<a {if in_array($bodyclass, $active_classes)}class="active"{/if} href="index.php?action=content"><i class="fa fa-file-text-o"></i> Content</a>
					</li>
					<li class="settings">
						{$active_classes = ['settings']}
						<a {if in_array($bodyclass, $active_classes)}class="active"{/if} href="index.php?action=settings"><i class="fa fa-gear fa-fw"></i> Settings</a>
					</li>
					{*}
					<li>
						<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<a href="flot.html">Flot Charts</a>
							</li>
							<li>
								<a href="morris.html">Morris.js Charts</a>
							</li>
						</ul>
						<!-- /.nav-second-level -->
					</li>
					<li>
						<a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
					</li>
					<li>
						<a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<a href="panels-wells.html">Panels and Wells</a>
							</li>
							<li>
								<a href="buttons.html">Buttons</a>
							</li>
							<li>
								<a href="notifications.html">Notifications</a>
							</li>
							<li>
								<a href="typography.html">Typography</a>
							</li>
							<li>
								<a href="icons.html"> Icons</a>
							</li>
							<li>
								<a href="grid.html">Grid</a>
							</li>
						</ul>
						<!-- /.nav-second-level -->
					</li>
					<li>
						<a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<a href="#">Second Level Item</a>
							</li>
							<li>
								<a href="#">Second Level Item</a>
							</li>
							<li>
								<a href="#">Third Level <span class="fa arrow"></span></a>
								<ul class="nav nav-third-level">
									<li>
										<a href="#">Third Level Item</a>
									</li>
									<li>
										<a href="#">Third Level Item</a>
									</li>
									<li>
										<a href="#">Third Level Item</a>
									</li>
									<li>
										<a href="#">Third Level Item</a>
									</li>
								</ul>
								<!-- /.nav-third-level -->
							</li>
						</ul>
						<!-- /.nav-second-level -->
					</li>
					<li>
						<a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<a href="blank.html">Blank Page</a>
							</li>
							<li>
								<a href="login.html">Login Page</a>
							</li>
						</ul>
						<!-- /.nav-second-level -->
					</li>
					{*}
				</ul>
			</div>
			<!-- /.sidebar-collapse -->
		</div>
		<!-- /.navbar-static-side -->
	</nav>

	<div id="page-wrapper">
		{block name=inner_content}
			{if $admin}
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
			<div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Welcome
                        </div>
                        <div class="panel-body">
							<small>Hello {$admin->getName()}</small>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
			{/if}
		{/block}
	</div>
	<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
{/block}