{extends file='dashboard.tpl'}

{block name=inner_content}

	{include file='errors.tpl' errors=$errors}
	
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				{if $history}
					Seeing archived version of content '{$content->getName()}' from <strong>{$content->getModified()|date_format:$config.datetime}</strong>
				{else}
					{if $content}Edit Content '{$content->getName()}'{else}Add New Content on page '{$selected->getName()}'{/if}
				{/if}
			</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
				
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-8">
							<form role="form" method="post">
							
								<div class="form-group">
									<div class="checkbox">
										<label>
											<input type="checkbox" value="1" name="active" {if $history}disabled{/if} {if !$content || $content->getActive()}checked{/if}>Active
										</label>
									</div>
								</div>
								
								{if $content}
									{$selected_id = $content->getPage()}
								{else}
									{$selected_id = $selected->getId()}
								{/if}
								
								{include file="snippets/select_page.tpl" pages=$pages selected=$selected_id}
								
								<div class="form-group">
									<label>Name</label>
									<input class="form-control" name="name" value="{if $content}{$content->getName()}{/if}" {if $history}disabled{/if}>
								</div>
								
								<div class="form-group">
									<div class="checkbox">
										<label>
											<input type="checkbox" value="1" name="showname" {if $history}disabled{/if} {if !$content || $content->getShowname()}checked{/if}>Show Title
										</label>
									</div>
								</div>
								
								<div class="form-group">
									<label>Type</label>
									<select class="form-control" name="type" {if isset($history) && $history}disabled{/if}>
										{foreach from=$content_types key=type item=typename}
											<option {if $content && $content->getType() == $type} selected {/if} value="{$type}" >{$typename}</option>
										{/foreach}
									</select>
								</div>
								
								<div class="form-group">
									<label>Size</label>
									<select class="form-control" name="columns" {if isset($history) && $history}disabled{/if}>
										{foreach from=$column_sizes key=columns item=columnname name=columns_loop}
											<option {if ($content && $content->getColumns() == $columns) || (!$content && $smarty.foreach.columns_loop.last)} selected {/if} value="{$columns}" >{$columnname}</option>
										{/foreach}
									</select>
								</div>
								
								
								
								<div class="form-group">
									<label>Text</label>
									<textarea class="form-control" name="text" id="text" {if isset($history) && $history}disabled{/if}>{if $content}{$content->getText()}{/if}</textarea>
								</div>
								
								{if $content}
									<div class="form-group row">
										<div class="col-sm-2">
											<label>Created</label>
										</div>
										<div class="col-sm-2">
											<input disabled value="{$content->getCreated()|date_format:$config.datetime}" />
										</div>
									</div>
									
									<div class="form-group row">
										<div class="col-sm-2">
											<label>Last Modified</label>
										</div>
										<div class="col-sm-2">
											<input disabled value="{$content->getModified()|date_format:$config.datetime}" />
										</div>
									</div>
								{/if}
								
								{if $history}
									<input type="hidden" name="historyid" value="{$history}" />
									<input type="hidden" name="page" value="{$content->getPage()}" />
									<button type="submit" name="do" value="restore_content" class="btn btn-default">Restore</button>
								{else}
									<button type="submit" name="do" value="{if $content}edit_content{else}add_content{/if}" class="btn btn-default">Save</button>
									<button type="reset" class="btn btn-default">Reset</button>
								{/if}
							</form>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
{/block}