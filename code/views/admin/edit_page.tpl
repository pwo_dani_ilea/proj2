{extends file='dashboard.tpl'}

{block name=inner_content}

	{include file='errors.tpl' errors=$errors}
	
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				{if $history}
					Seeing archived version of page '{$page->getName()}' from <strong>{$page->getModified()|date_format:$config.datetime}</strong>
				{else}
					{if $page}Edit Page '{$page->getName()}'{else}Add New Page{/if}
				{/if}
			</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
				
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-8">
							<form role="form" method="post">
							
								<div class="form-group">
									<div class="checkbox">
										<label>
											<input type="checkbox" value="1" name="active" {if $history}disabled{/if} {if $page && $page->getActive()}checked{/if}>Active
										</label>
									</div>
								</div>
								
								{if count($parents)}
									{if !$page || !$page->getSubpages()}
									<div class="form-group">
										<label>Parent</label>
										<select class="form-control" name="parent" {if $history}disabled{/if}>
												<option value="0">-</option>
											{foreach from=$parents item=parent}
												{if $page && $page->getId() == $parent->getId()}
												{else}
													<option value="{$parent->getId()}" {if $page && $page->getParent() == $parent->getId()} selected {/if}>{$parent->getName()}</option>
												{/if}
											{/foreach}
										</select>
									</div>
									{/if}
								{/if}
								
								<div class="form-group">
									<label>Name</label>
									<input class="form-control" name="name" value="{if $page}{$page->getName()}{/if}" {if $history}disabled{/if}>
								</div>
								
								{if $page}
									<div class="form-group row">
										<div class="col-sm-2">
											<label>Created</label>
										</div>
										<div class="col-sm-2">
											<input disabled value="{$page->getCreated()|date_format:$config.datetime}" />
										</div>
									</div>
									
									<div class="form-group row">
										<div class="col-sm-2">
											<label>Last Modified</label>
										</div>
										<div class="col-sm-2">
											<input disabled value="{$page->getModified()|date_format:$config.datetime}" />
										</div>
									</div>
								{/if}
								
								{if $history}
									<input type="hidden" name="historyid" value="{$history}" />
									<button type="submit" name="do" value="restore_page" class="btn btn-default">Restore</button>
								{else}
									<button type="submit" name="do" value="{if $page}edit_page{else}add_page{/if}" class="btn btn-default">Save</button>
									<button type="reset" class="btn btn-default">Reset</button>
								{/if}
							</form>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
{/block}