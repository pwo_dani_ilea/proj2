{if count($errors) > 0}
<div class="row error_holder">
	<div class="col-lg-12">
		<ul class="errors">
			{foreach $errors as $error}
			<li>{$error}</li>
			{/foreach}
		</ul>
	</div>
	<!-- /.col-lg-12 -->
</div>
{/if}