{extends file='dashboard.tpl'}

{block name=inner_content}
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Log of changes for '{$object->getName()}'
			</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables">
							<thead>
								<tr>
									<th>ID</th>
									<th>Modified</th>
									<th>Changed Fields</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>								
								{foreach from=$history item=history_object name=list_history}
									<tr class="page {if $smarty.foreach.list_history.index %2}my_odd{else}my_even{/if}">
										<td>{$history_object->getId()}</td>
										<td>{$history_object->getCreated()|date_format:$config.datetime}</td>
										<td>{$history_object->getFields()}</td>
										<td class="center actions">
											<a href="index.php?action=editHistory&id={$history_object->getId()}" class="edit_entry" title="See Modifications"> <i class="fa fa-eye"></i></a> 
											<form class="delete_object" method="post">
												<input type="hidden" name="do" value="delete_history" />
												<input type="hidden" name="id" value="{$history_object->getId()}" />
												<input type="hidden" name="table" value="{$object->getTable()}" />
												<a class="delete_entry" title="Delete Log Entry"> <i class="fa fa-times"></i></a>
											</form>
											
										</td>
									</tr>
								{/foreach}
							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
					
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
{/block}