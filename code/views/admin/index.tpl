<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{block name=title}CMS Administration{/block}</title>
		<base href="{$baseHref}" >
		
		<link href="{$adminViewsPath}/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="{$adminViewsPath}/css/metisMenu.css" rel="stylesheet" media="screen">
		<link href="{$adminViewsPath}/css/dataTables.bootstrap.css" rel="stylesheet" media="screen">
		<link href="{$adminViewsPath}/css/dataTables.responsive.css" rel="stylesheet" media="screen">
		<link href="{$adminViewsPath}/css/font-awesome.min.css" rel="stylesheet" media="screen">
		<link href="{$adminViewsPath}/css/admin.css" rel="stylesheet" media="screen">

	</head>
	<body class="{$bodyclass}">
	
		<div class="container error_holder">
		{if isset($errors) && count($errors)}
			<ul class="errors">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
		{/if}
		</div>
		{block name=dashboard}{/block}
		
		<script type="text/javascript" src="{$adminViewsPath}/js/jquery.min.js"></script>
		<script type="text/javascript" src="{$adminViewsPath}/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="{$adminViewsPath}/js/metisMenu.min.js"></script>
		<script type="text/javascript" src="{$adminViewsPath}/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="{$adminViewsPath}/js/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="{$adminViewsPath}/js/admin.js"></script>
		<script type="text/javascript" src="{$adminViewsPath}/js/bootbox.min.js"></script>
		<script type="text/javascript" src="{$adminViewsPath}/js/bootstrap-waitingfor.js"></script>
		
		<script type="text/javascript" src="../plugins/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="../plugins/ckfinder/ckfinder.js"></script>
		
		<script>
		$(document).ready(function() {
			$('#dataTables').DataTable({
					responsive: true,
					"iDisplayLength": 1000,
					"aaSorting": []
			});
		});
		</script>
		<script>
			if($('#text').length) {
				CKEDITOR.config.extraPlugins = 'autogrow,image';
				CKEDITOR.config.autoGrow_onStartup = true;
				CKEDITOR.config.autoGrow_bottomSpace = 50;
				var editor = CKEDITOR.replace( 'text' );
				CKFinder.setupCKEditor( editor, '/plugins/ckfinder/' );
			}
		</script>
	</body>
</html>