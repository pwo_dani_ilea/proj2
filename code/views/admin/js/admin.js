$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
		
		$('.sidebar').css('height', $(window).height());
    });

    /* var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0 ;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    } */
});

$(function(){
	$('#admin_logout').click(function(){
		$('#logout_form').submit();
	});
	
	$(document).on('click','.toggle_active', function(){
		var toggledElement = $(this);
		var params = {table: toggledElement.data('table'), id: toggledElement.data('id')};
		adminAjax('toggleActive',params, 'html', function(result){
			toggledElement.parent().html(result);
		});
	});
	
	$(document).on('click','.delete_entry', function(){
		var deleteElement = $(this);
		bootbox.confirm("Are you sure?", function(result) {
			if(result == true) {
				deleteElement.parents('form').submit();
			}
		}); 
	});
	
	$(document).on('click','.copy_entry', function(){
		var copyElement = $(this);
		copyElement.parents('form').submit();
			
	});
	
	$(document).on('click','.paste_entry', function(){
		var pasteElement = $(this);
		pasteElement.parents('form').submit();
			
	});
	
	$(document).on('change','#enabled_placeholder', function(){
		if($(this).is(':checked')) {
			$('#enabled_real').val('1');
		}
		else {
			$('#enabled_real').val('0');
		}
	});
	
	$(document).on('change','#change_page select', function(){
		$('#change_page').submit();
	});
	
	$(document).on('click','#regenerate_urls', function(){
		waitingDialog.show('Regenerating...', {dialogSize: 'sm', progressType: 'warning'});
		adminAjax('regenerateUrls', {}, 'HTML', function(result){
			waitingDialog.hide();
		});
	});
	
	var sortedRows = false;
	
	$(document).on('click', '.reorder_holder a', function(){
		
		if(sortedRows)
			return;
			
		sortedRows = true;
		
		var tr = $(this).parents('tr');
		
		if($(this).hasClass('move_down')) {
			if(tr.hasClass('page')) {
				var firstSiblingPage = tr.nextAll('.page').first().data('page');
				$('tr[data-page="'+tr.data('page')+'"], tr[data-parent="'+tr.data('page')+'"]').insertAfter($('tr[data-page="'+firstSiblingPage+'"], tr[data-parent="'+firstSiblingPage+'"]').last());
			}
			else {
				$('tr[data-subpage="'+tr.data('subpage')+'"]').insertAfter(tr.next());
			}
		}
		else {
			if(tr.hasClass('page')) {
				var firstSiblingPage = tr.prevAll('.page').first().data('page');
				$('tr[data-page="'+tr.data('page')+'"], tr[data-parent="'+tr.data('page')+'"]').insertBefore($('tr[data-page="'+firstSiblingPage+'"], tr[data-parent="'+firstSiblingPage+'"]').first());
			}
			else {
				$('tr[data-subpage="'+tr.data('subpage')+'"]').insertBefore(tr.prev());
			}
		}
		
		var orderings = {};
		
		$('tr[data-page]').each(function(index){
			orderings[$(this).data('page')] = index;
			
			$('tr[data-parent="'+$(this).data('page')+'"]').each(function(index){
				orderings[$(this).data('subpage')] = index;
			});
		});
		
		var action = 'orderPages';
		if(tr.hasClass('content')) {
			action = 'orderContent';
		}
		
		adminAjax(action, {order: orderings}, 'JSON', function(result){
			window.location = window.location.href;
		});
			
	});
});


function adminAjax(action, parameters, dataType, callback) {
	if(typeof dataType == 'undefined') {
		dataType = 'html';
	}
	$.ajax({
		url: '/admin/index.php?do=ajax&action='+action,
		method: 'POST',
		data: parameters,
		dataType: dataType,
		success: function(result) {
			callback(result);
		},
		fail: function(res) {
			console.debug(res);
		}
	});
}