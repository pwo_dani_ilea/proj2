{extends file='index.tpl'}

{block name=dashboard}
<div class="container">

	<form class="form-signin" action="" method="post" target="_self">
		<h2 class="form-signin-heading">Please sign in</h2>
		<input name="username" type="text" autofocus="" required="" placeholder="User name" class="form-control">
		<input name="password" type="password" required="" placeholder="Password" class="form-control">
	
		<input type="hidden" name="do" value="login" />
		<button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
    </form>

</div>
{/block}