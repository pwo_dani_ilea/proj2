{extends file='dashboard.tpl'}

{block name=inner_content}
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Pages
				<a class="add_new" href="index.php?action=addPage">Add New <i class="fa fa-plus fa-fw"></i></a>
			</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables">
							<thead>
								<tr>
									<th>ID</th>
									<th>Active</th>
									<th>Name</th>
									<th>Created</th>
									<th>Modified</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>								
								{foreach from=$pages item=page name=list_pages}
									<tr class="page {if $smarty.foreach.list_pages.index %2}my_odd{else}my_even{/if}" data-page="{$page->getId()}">
										<td>{$page->getId()}</td>
										<td>{include file="snippets/toggle_active.tpl" object=$page}</td>
										<td class="center">{$page->getName()}</td>
										<td>{$page->getCreated()|date_format:$config.datetime}</td>
										<td>{$page->getModified()|date_format:$config.datetime}</td>
										<td class="center actions">
											<a href="index.php?action=editPage&id={$page->getId()}" class="edit_entry" title="Edit Page"> <i class="fa fa-pencil-square-o"></i></a> 
											<form class="delete_object" method="post">
												<input type="hidden" name="do" value="delete_page" />
												<input type="hidden" name="id" value="{$page->getId()}" />
												<a class="delete_entry" title="Delete Page"> <i class="fa fa-times"></i></a>
											</form>
											<a href="index.php?action=history&table=page&objectid={$page->getId()}" class="history_entry" title="See Page History"> <i class="fa fa-history"></i></a> 
											
											<a href="{pagelink id=$page->getId()}" target="_blank" class="view_page" title="Edit Page"> <i class="fa fa-eye"></i></a> 
											
											{if count($pages) > 1}
												<span class="reorder_holder">
												{if !$smarty.foreach.list_pages.last}
													<a class="move_down" data-id="{$page->getId()}" title="Move Down"><i class="fa fa-arrow-down"></i></a>
												{/if}
												{if !$smarty.foreach.list_pages.first}
													<a class="move_up" data-id="{$page->getId()}"  title="Move Up"><i class="fa fa-arrow-up"></i></a>
												{/if}
												</span>
											{/if}
										</td>
									</tr>
									{if $page->getSubpages()}
										{assign var="subpages" value=$page->getSubpages()}
										{foreach from=$subpages item=sub_page name=sub_pages}
											<tr class="sub_page {if $smarty.foreach.sub_pages.index %2}my_odd{else}my_even{/if}" data-parent="{$page->getId()}" data-subpage="{$sub_page->getId()}">
												<td>{$sub_page->getId()}</td>
												<td>{include file="snippets/toggle_active.tpl" object=$sub_page}</td>
												<td class="center">{$sub_page->getName()}</td>
												<td>{$sub_page->getCreated()|date_format:$config.datetime}</td>
												<td>{$sub_page->getModified()|date_format:$config.datetime}</td>
												<td class="center actions">
													<a href="index.php?action=editPage&id={$sub_page->getId()}" class="edit_entry" title="Edit Page"> <i class="fa fa-pencil-square-o"></i></a> 
													<form class="delete_object" method="post">
														<input type="hidden" name="do" value="delete_page" />
														<input type="hidden" name="id" value="{$sub_page->getId()}" />
														<a class="delete_entry" title="Delete Page"> <i class="fa fa-times"></i></a>
													</form>
													<a href="index.php?action=history&table=page&objectid={$page->getId()}" class="history_entry" title="See Page History"> <i class="fa fa-history"></i></a> 
													
													<a href="{pagelink id=$sub_page->getId()}" target="_blank" class="view_page" title="Edit Page"> <i class="fa fa-eye"></i></a>
													
													{if count($subpages) > 1}
														<span class="reorder_holder">
														{if !$smarty.foreach.sub_pages.last}
															<a class="move_down" data-id="{$sub_page->getId()}" title="Move Down"><i class="fa fa-arrow-down"></i></a>
														{/if}
														{if !$smarty.foreach.sub_pages.first}
															<a class="move_up" data-id="{$sub_page->getId()}" title="Move Up"><i class="fa fa-arrow-up"></i></a>
														{/if}
														</span>
													{/if}
												</td>
											</tr>
										{/foreach}
									{/if}
								{/foreach}
							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
					
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
{/block}