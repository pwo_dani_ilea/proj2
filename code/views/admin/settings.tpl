{extends file='dashboard.tpl'}

{block name=inner_content}

	{include file='errors.tpl' errors=$errors}
	
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Website Settings
			</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-8">
							<button id="regenerate_urls" class="btn btn-default">Regenerate URLs</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
				
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-8">
							<form role="form" method="post">
							
								<div class="form-group">
									<div class="checkbox">
										<label>
											<input id="enabled_placeholder" type="checkbox" {if $settings.enabled}checked{/if}> Enable
										</label>
									</div>
								</div>
																
								<div class="form-group">
									<label>Website Name </label>
									<input class="form-control" name="site_name" value="{$settings.site_name}" />
								</div>
																
								<div class="form-group">
									<label>Website Description </label>
									<input class="form-control" name="site_description" value="{$settings.site_description}" />
								</div>
																
								<div class="form-group">
									<label>Website Author </label>
									<input class="form-control" name="site_author" value="{$settings.site_author}" />
								</div>
																
								<div class="form-group">
									<label>Website Year </label>
									<input class="form-control" name="site_year" value="{$settings.site_year}" />
								</div>
																
								<div class="form-group">
									<label>URL Ending</label>
									<input class="form-control" name="url_ending" value="{$settings.url_ending}" />
								</div>
																
								<div class="form-group">
									<label>URL Space Character</label>
									<input class="form-control" name="url_space" value="{$settings.url_space}" />
								</div>
								
								<div class="form-group">
									<label>Home Page </label>
									<select class="form-control" name="home_page">
										<option value="0">-</option>
										{foreach from=$pages item=page}
											<option value="{$page->getId()}" {if $settings.home_page eq $page->getId()}selected{/if}> {$page->getName()}</option>
											{if $page->getSubpages()}
												{foreach from=$page->getSubpages() item=subpage}
													<option value="{$subpage->getId()}" {if $settings.home_page eq $subpage->getId()}selected{/if}> &nbsp;&nbsp;{$subpage->getName()}</option>
												{/foreach}
											{/if}
										{/foreach}										
									</select>
									<span>(if empty first page will be used)</span>
								</div>
								
								<div class="form-group">
									<label>404 Page </label>
									<select class="form-control" name="missing_page">
										<option value="0">-</option>
										{foreach from=$pages item=page}
											<option value="{$page->getId()}" {if $settings.missing_page eq $page->getId()}selected{/if}> {$page->getName()}</option>
											{if $page->getSubpages()}
												{foreach from=$page->getSubpages() item=subpage}
													<option value="{$subpage->getId()}" {if $settings.missing_page eq $subpage->getId()}selected{/if}> &nbsp;&nbsp;{$subpage->getName()}</option>
												{/foreach}
											{/if}
										{/foreach}										
									</select>
									<span>(if empty homepage will be used)</span>
								</div>
								
								<input type="hidden" name="enabled" id="enabled_real" value="{$settings.enabled}" />
								<button type="submit" name="do" value="edit_settings" class="btn btn-default">Save</button>
								
							</form>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
{/block}