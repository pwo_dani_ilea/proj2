<div class="form-group">
	<label>Page</label>
	<select class="form-control" name="page" {if isset($history) && $history}disabled{/if}>
		{if $pages}
			{foreach from=$pages item=page}
				<option {if isset($selected) && $selected == $page->getId()} selected {/if} value="{$page->getId()}" >{$page->getName()}</option>
				{if $page->getSubpages()}
					{foreach from=$page->getSubpages() item=subpage}
						<option {if isset($selected) && $selected == $subpage->getId()} selected {/if} value="{$subpage->getId()}" >&nbsp;&nbsp;{$subpage->getName()}</option>
					{/foreach}
				{/if}
			{/foreach}
		{/if}
	</select>
</div>