<a class="toggle_active" data-table="{$object->getTable()}" data-id="{$object->getId()}">
	{if $object->getActive()}
		<i class="fa fa-check"></i>
	{else}
		<i class="fa fa-close"></i>
	{/if}
</a>