<div class="col-md-{if $content->getColumns()}{$content->getColumns()}{else}12{/if}" id="c{$content->getId()}">
	<div class="panel panel-default">
		{if $content->getShowname()}
		<div class="panel-heading">
			<h4><i class="fa fa-fw fa-check"></i> {$content->getName()}</h4>
		</div>
		{/if}
		<div class="panel-body">
			{$content->getText()}
			{if $edit}
				<span class="edit_content" data-content="{$content->getId()}"><i class="fa fa-pencil-square-o"></i></span>
			{/if}
		</div>
	</div>
</div>