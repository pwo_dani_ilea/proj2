<div class="col-md-{$content->getColumns()}" id="c{$content->getId()}">
	<div class="row">
		{if $content->getShowname()}
		<div class="col-md-12">
			<h2 class="page-header" {if $edit}contenteditable="true" data-content="{$content->getId()}"{/if}>{$content->getName()}</h2>
		</div>
		{/if}
		<div class="col-md-12 text_holder">
			{$content->getText()}
			{if $edit}
				<span class="edit_content edit_title" data-content="{$content->getId()}"><i class="fa fa-pencil-square-o"></i></span>
			{/if}
		</div>
	</div>
</div>

