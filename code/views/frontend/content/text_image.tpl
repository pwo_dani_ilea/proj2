<div class="col-md-{$content->getColumns()}" id="c{$content->getId()}">
	<div class="row">
		{if $content->getName()}
		<div class="col-lg-12">
			<h2 class="page-header">{$content->getName()}</h2>
		</div>
		{/if}
	</div>
	<div class="row text_image_holder">
		<div class="col-md-6 text_holder">
			{$content->getText()}
		</div>
		<div class="col-md-6 image_holder">

		</div>
		{if $edit}
		<span class="edit_content" data-content="{$content->getId()}"><i class="fa fa-pencil-square-o"></i></span>
		{/if}
	</div>
</div>