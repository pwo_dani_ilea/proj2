<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{$settings.site_author}">
    <meta name="author" content="{$settings.site_description}">

    <title>{$settings.site_name} - {$page->getName()}</title>

    <!-- Bootstrap Core CSS -->
    <link href="{$frontViewsPath}/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{$frontViewsPath}/css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{$frontViewsPath}/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    {include file="menu/navigation.tpl"}
	
    <!-- Page Content -->
    <div class="container">
		
		<div class="row">
			{$content}
		</div>
		
		<hr/>
		
		<!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; {$settings.site_name} {$settings.site_year}</p>
                </div>
            </div>
        </footer>
	</div>
  
    <!-- jQuery -->
    <script src="{$frontViewsPath}/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{$frontViewsPath}/js/bootstrap.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{$frontViewsPath}/js/script.js"></script>

    <!-- Script to Activate the Carousel -->
   
	{if $edit}
	
	<script type="text/javascript" src="../plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="../plugins/ckfinder/ckfinder.js"></script>
	
	<script>
		CKEDITOR.config.extraPlugins = 'autogrow,image';
		CKEDITOR.config.autoGrow_onStartup = true;
		CKEDITOR.config.autoGrow_bottomSpace = 50;
	</script>
	
	{/if}
   
</body>

</html>
