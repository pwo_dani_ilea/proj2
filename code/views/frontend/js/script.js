$('.slide').each(function(index){
	
	if($(this).find('img').length > 0) {
		
		var htmlCode = '<div class="carousel-inner">';
		$(this).find('img').each(function(index){
			var img = $(this);
			var active = '';
			if(index == 0) {
				active = 'active';
			}
			htmlCode += '<div class="item '+active+'"> <div class="fill" style="background-image:url(\''+img.attr('src')+'\');"></div>  <div class="carousel-caption"><h2>'+img.attr('alt')+'</h2></div> </div> ';
		});
		htmlCode += '<a class="left carousel-control" href="'+'#carousel'+index+'" data-slide="prev"><span class="icon-prev"></span></a> <a class="right carousel-control" href="'+'#carousel'+index+'" data-slide="next"><span class="icon-next"></span></a></div>';
		
		$(this).html(htmlCode).attr('id', 'carousel'+index);
		$(this).carousel();
	}
	else {
		$(this).hide();
	}
});

$('.text_holder').each(function(){
	var contentElement = $(this);
	contentElement.find('img').each(function(){
		$(this).appendTo(contentElement.next());
	});
});

$(document).on('click', '.edit_content', function(){
	var editButton = $(this);
	var contentId = editButton.data('content');
	if(contentId) {
		adminAjax('getContentText', {id: contentId}, 'html', function(result){
			editButton.parent().html('<textarea id="edit_'+contentId+'">'+result+'</textarea><span class="save_content" data-content="'+contentId+'"><i class="fa fa-floppy-o"></i></span><span class="cancel_content" data-content="'+contentId+'"><i class="fa fa-times"></i></span>');
			/* var editor = CKEDITOR.replace( 'edit_'+contentId, {
				toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			
					'/',																
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'insert'}
				]
			}); */
			var editor = CKEDITOR.replace( 'edit_'+contentId);
			CKFinder.setupCKEditor( editor, '/plugins/ckfinder/' );
			
		});
	}
});

$(document).on('click', '.save_content', function(){
	var saveButton = $(this);
	var contentId = saveButton.data('content');
	if(contentId) {
		if(typeof CKEDITOR.instances['edit_'+contentId] != 'undefined') {
			adminAjax('saveContentText', {id: contentId, text: CKEDITOR.instances['edit_'+contentId].getData()}, 'html', function(result){
				//window.location = window.location.origin + window.location.pathname + '#c'+contentId;
				window.location.reload();
			});
		}
	}
});


$(document).on('click', '.cancel_content', function(){
	var saveButton = $(this);
	var contentId = saveButton.data('content');
	window.location.reload();
});


function adminAjax(action, parameters, dataType, callback) {
	if(typeof dataType == 'undefined') {
		dataType = 'html';
	}
	$.ajax({
		url: window.location.origin+'/admin/index.php?do=ajax&action='+action,
		method: 'POST',
		data: parameters,
		dataType: dataType,
		success: function(result) {
			callback(result);
		},
		fail: function(res) {
			console.debug(res);
		}
	});
}