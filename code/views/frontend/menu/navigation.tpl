<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				{foreach from=$menu_pages item=page}
					<li>
						{if isset($page->sub_pages)}
							<a href="/{$page->getUrl()}" class="dropdown-toggle" data-toggle="dropdown">{$page->getName()} <b class="caret"></b></a>
							{foreach from=$page->sub_pages item=subpage}
								 <ul class="dropdown-menu">
									<li>
										<a href="/{$subpage->getUrl()}">{$subpage->getName()}</a>
									</li>
								 </ul>
							{/foreach}
						{else}
							<a href="/{$page->getUrl()}">{$page->getName()}</a>
						{/if}
					</li>
					
				{/foreach}
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>