<?php

/* START CONSTANTS */

define("DEV_MODE", true);
//define("ADMIN_MODE", true);
define("SLASH", '/');
define("ROOT", dirname(__FILE__).'/');

if(DEV_MODE) {
	ini_set('display_errors', '1');
	error_reporting(E_ALL);
}

/* Database Connection */
define("DB_HOST", "localhost");
define("DB_USER", "dani_cms");
define("DB_PASS", "Danutz02");
define("DB_NAME", "dani_cms");

/*Configuration Constants*/
define("DATE_FORMAT", '%d/%m/%Y');
define("TIME_FORMAT", '%H:%M:%S');
define("DATE_TIME_FORMAT", '%H:%M:%S %d/%m/%Y');

/* END CONSTANTS */

session_start();

/* Autoload Behavior for classes */
function __autoload($name) {
	
	if(is_file(ROOT.'code/controllers/'.strtolower($name).'.php')) {
		require_once ROOT.'code/controllers/'.strtolower($name).'.php';
	}
	
	if(is_file(ROOT.'code/models/'.strtolower($name).'.php')) {
		require_once ROOT.'code/models/'.strtolower($name).'.php';
	}
	
	if(is_file(ROOT.'code/models/repository/'.strtolower($name).'.php')) {
		require_once ROOT.'code/models/repository/'.strtolower($name).'.php';
	}
	
	if(is_file(ROOT.'code/'.strtolower($name).'.php')) {
		require_once ROOT.'code/'.strtolower($name).'.php';
	}
}

/* All-purpose debugging function */
function dump($var) {
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
}


spl_autoload_register("__autoload");

/* SMARTY Configuration */
require_once('libraries/smarty/Smarty.class.php');
/* $smarty = new Smarty();
$smarty->setTemplateDir('templates/');
$smarty->setCompileDir('tmp/smarty_compile/');
$smarty->setConfigDir('config/');
$smarty->setCacheDir('cache/'); */


$GLOBALS['DB'] = DB::getInstance();

?>