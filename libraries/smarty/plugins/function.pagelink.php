<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.pagelink.php
 * Type:     function
 * Name:     pagelink
 * Purpose:  outputs a page link
 * -------------------------------------------------------------
 */
function smarty_function_pagelink($params, &$smarty)
{
    if(isset($params['id']) && $params['id']) {
		$page = new Page($params['id']);
		if($page && $page->getId()) {
			return 'http://'.$_SERVER['HTTP_HOST'].'/'.$page->getUrl();
		}
	}
	
	return '';
}
?>
